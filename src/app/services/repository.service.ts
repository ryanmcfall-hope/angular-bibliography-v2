import { Injectable } from '@angular/core';
import { Bibentry } from '../models/bibentry.model';
import { Author } from '../models/author.model';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { of } from 'rxjs/observable/of';
import { timer } from 'rxjs/observable/timer';
import { catchError, map } from 'rxjs/operators';

import { HttpClient, HttpErrorResponse } from "@angular/common/http";

export interface BookResponse {
  id : number;
  title: string;
  year: number;
  publisher: string;
  authorId : number;
  author: Author;
}

@Injectable()
export class RepositoryService {

  private books : Bibentry[];

  static baseUrl = 'http://cs49501.cs.hope.edu:3000';

  constructor(private httpClient : HttpClient) {
    this.books = new Array<Bibentry>();

    let wmd : Bibentry  = new Bibentry();
    wmd.title = "Weapons of Math Destruction";
    wmd.authorFirst = 'cathy';
    wmd.authorLast = "O'Neil";
    wmd.year = 2016;
    wmd.publisher = "Broadway Books";
    wmd.id = 1;

    let angular : Bibentry = new Bibentry();
    angular.title = "Pro Angular";
    angular.authorFirst = 'Adam';
    angular.authorLast = "Freeman";
    angular.year = 2017;
    angular.publisher = "Apress";
    angular.id = 2;

    let cnh : Bibentry = new Bibentry();
    cnh.authorFirst = 'Bill';
    cnh.authorLast = 'Watterson';
    cnh.title = 'Calvin & Hobbes';
    cnh.year = 1987;
    cnh.publisher = 'Andrews McMeel Publishing';
    cnh.id = 3;

    this.books.push(cnh);
    this.books.push(wmd);
    this.books.push(angular);
  }


  createBibentryArrayFromJson(entries : BookResponse[]) : Array<Bibentry>  {
      return entries.map(entry => this.createBibentryFromJson(entry));
  }

  createBibentryFromJson(json : BookResponse): Bibentry {
    let entry : Bibentry = new Bibentry();
    entry.id = json.id;
    entry.title = json.title;
    entry.year = json.year;
    entry.publisher = json.publisher;
    entry.authorFirst = json.author.firstName;
    entry.authorLast = json.author.lastName;
    return entry;
  }

  private handleError (error :  HttpErrorResponse) : ErrorObservable  {
    let errorMessage : string = "An unknown error occurred.";

    if (error.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${error.error.message}`;
    }
    else {
      switch (error.status) {
        case 0:
          errorMessage = 'Unable to connect to the database server';
          break;
        default:
          errorMessage = `Backend returned code ${error.status}, body was ${error.error}`
      }
    }
    console.error(errorMessage);
    return new ErrorObservable(errorMessage);
  }

  get allBooks () : Observable<Bibentry[]> {
    let url = `${RepositoryService.baseUrl}/books?_expand=author`;
    return this.httpClient.get<BookResponse[]>(url).pipe (
      map( (entries : BookResponse[]) => this.createBibentryArrayFromJson(entries)),
      catchError(this.handleError)
    );
  }

  addBook(book : Bibentry) : void {
    let currentMax : Bibentry = this.books[0]
    currentMax = this.books.reduce ((currentMax, element) => element.id > currentMax.id ? element : currentMax );
    book.id = currentMax.id + 1;
    console.log(`New book's ID set to ${book.id}`);
    this.books.push(book);
  }

  getBookById(id : number) : Observable<Bibentry> {
    let url = `${RepositoryService.baseUrl}/books/${id}?_expand=author`;
    return this.httpClient.get<BookResponse>(url).pipe(
      map( (data : BookResponse)  => this.createBibentryFromJson(data) )
    );
  }

  updateBook (book: Bibentry) : Observable<boolean> {
    //  Create a timer that will go off after 1 second
    const source = timer(1000);

    //  The timer gives us a single value of 0, which we'll map to true or false depending
    //  on a special value for the title
    return source.pipe (map (val => book.title === 'FAIL' ? false : true));

  }
}
