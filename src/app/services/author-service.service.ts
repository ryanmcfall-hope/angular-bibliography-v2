import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Author } from '../models/author.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthorServiceService {

  constructor(private httpClient : HttpClient) { }

  addAuthor(firstName : string, lastName: string) : Observable<Author> {
    let url: string = "http://cs49501.cs.hope.edu:3000/authors";

    let author: Author = new Author(firstName, lastName);

    return this.httpClient.post<Author>(url, author);
  }

  getAuthors() : Observable<Author[]>{
    let url: string = "http://cs49501.cs.hope.edu:3000/authors";

    return this.httpClient.get<Author[]>(url);
    //let authorArray = new Array[]
  }
}
