import { Component, OnInit } from '@angular/core';
import { Author } from '../../models/author.model';
import { AuthorServiceService } from '../../services/author-service.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {
  private author : Author;
  constructor(private authorService : AuthorServiceService, private router: Router) {
    this.author = new Author("","");
  }

  ngOnInit() {
  }

  addAuthor() {
    this.authorService.addAuthor(this.author.firstName, this.author.lastName).subscribe(author => this.router.navigateByUrl("/authors"));
  }

}
