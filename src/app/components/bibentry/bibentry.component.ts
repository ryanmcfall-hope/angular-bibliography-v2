import { Component, OnInit, Input } from '@angular/core';
import { Bibentry } from '../../models/bibentry.model';

@Component({
  selector: 'bibentry',
  templateUrl: './bibentry.component.html',
  styleUrls: ['./bibentry.component.css']
})
export class BibentryComponent implements OnInit {

  @Input() private book : Bibentry;

  constructor() {
  }

  ngOnInit() {
  }

  transformAuthor(authorFirst : string, authorLast: string) : string {
    return `${authorLast}, ${authorFirst.charAt(0).toUpperCase()}`;
  }

}
