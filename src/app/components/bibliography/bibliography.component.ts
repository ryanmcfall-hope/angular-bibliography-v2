import { Component, OnInit } from '@angular/core';
import { RepositoryService } from '../../services/repository.service';
import { Bibentry } from '../../models/bibentry.model';

import { Observable } from 'rxjs/Observable';
import { timer } from 'rxjs/observable/timer';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-bibliography',
  templateUrl: './bibliography.component.html',
  styleUrls: ['./bibliography.component.css']
})
export class BibliographyComponent implements OnInit {

  private books : Bibentry[];
  private apiError : boolean;
  private errorMessage: string;

  constructor(private _repository : RepositoryService) { }

  loadBooks () : void {
    this.apiError = false;
    this._repository.allBooks.subscribe (
      books => this.books = books,
      error => {
        this.apiError = true;
        this.errorMessage = error
      }
    );
  }

  ngOnInit() {
    const delayLoading = timer (2000);
    delayLoading.subscribe (data => this.loadBooks());
  }

}
