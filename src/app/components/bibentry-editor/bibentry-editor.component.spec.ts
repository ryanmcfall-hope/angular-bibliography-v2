import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BibentryEditorComponent } from './bibentry-editor.component';

describe('BibentryEditorComponent', () => {
  let component: BibentryEditorComponent;
  let fixture: ComponentFixture<BibentryEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BibentryEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BibentryEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
