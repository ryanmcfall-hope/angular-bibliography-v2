import { Component, OnInit, Input } from '@angular/core';

import { Author } from '../../models/author.model';

import {AuthorServiceService} from '../../services/author-service.service';

import { Router } from '@angular/router';
@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit {

  private authorList : Array<Author>;

  constructor(private authorServe : AuthorServiceService, private router : Router) {
    this.authorList = new Array<Author>();
  }

  ngOnInit() {
    this.authorServe.getAuthors().subscribe (
      authorList => this.authorList = authorList
    );
  }

  addAuthor() {
    this.router.navigateByUrl('/add-author');
  }
}
