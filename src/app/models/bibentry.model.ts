export class Bibentry {
  title : string;
  authorFirst : string;
  authorLast : string;
  year: number;
  isbn: string;
  publisher: string;
  id: number;
}
