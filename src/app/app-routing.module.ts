import { NgModule } from '@angular/core';

import { BibentryEditorComponent } from './components/bibentry-editor/bibentry-editor.component';
import { BibliographyComponent } from './components/bibliography/bibliography.component';

import { Routes, RouterModule } from '@angular/router';

import { AuthorListComponent } from './components/author-list/author-list.component';

import { AuthorComponent } from './components/author/author.component';

const routes: Routes = [
  {
    'path': 'new',
    'component': BibentryEditorComponent
  },
  {
    'path': 'edit/:id',
    'component': BibentryEditorComponent
  },
  {
    'path': '',
    'component': BibliographyComponent
  },
  {
    'path': 'authors',
    'component': AuthorListComponent
  },
  {
    'path': 'add-author',
    'component': AuthorComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
