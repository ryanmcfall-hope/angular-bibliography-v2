import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstInitial'
})
export class FirstInitialPipe implements PipeTransform {

  transform(value: any, args?: any): any {    
    if (typeof value === "string" || value instanceof String) {
        var stringValue = value as string;
        return stringValue.charAt(0);
    }
    else {
      return value;
    }
  }

}
