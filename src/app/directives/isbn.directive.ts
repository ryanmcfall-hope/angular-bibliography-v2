import { Directive, Input, OnInit } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[appIsbn]',
  providers: [{provide: NG_VALIDATORS, useExisting: IsbnDirective, multi: true}]
})
export class IsbnDirective implements Validator, OnInit {

  @Input("length") private lengthAttributeValue : string;
  private length : string;

  constructor() { }

  ngOnInit () {
    this.length = this.lengthAttributeValue ? this.lengthAttributeValue : "10";
    console.log(`Length set to ${this.length}`);
  }

  validate(control: AbstractControl): {[key: string]: any} {
    let controlValue = control.value;

    if (controlValue === null) {
      return null;
    }

    //  If the value isn't 10 characters yet, it can't be correct
    if (controlValue.length < 10) {
      return {'isbn': {message: 'Length must be 10 characters'}};
    }

    let i = 0;
    let sum = 0;
    let digit : number = 0;

    while (i < 9) {
      digit = +controlValue.substr(i, 1);
      sum += (i+1) * digit;
      i++;
    }

    let checkDigit : number = +controlValue.substr(9,1);
    let correctCheckDigit : number = sum % 11;
    if (correctCheckDigit === checkDigit) {
      return null;
    }

    return {
      'isbn': {message: `Check digit is ${checkDigit} but should be ${correctCheckDigit}`}
    }
  }

}
