import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BibentryComponent } from './components/bibentry/bibentry.component';
import { FirstInitialPipe } from './pipes/first-initial.pipe';
import { BibentryEditorComponent } from './components/bibentry-editor/bibentry-editor.component';
import { AppRoutingModule } from './/app-routing.module';
import { BibliographyComponent } from './components/bibliography/bibliography.component';

import { RepositoryService } from './services/repository.service';
import { AuthorListComponent } from './components/author-list/author-list.component';
import { AuthorServiceService } from './services/author-service.service';
import { AuthorComponent } from './components/author/author.component';
import { IsbnDirective } from './directives/isbn.directive';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    BibentryComponent,
    FirstInitialPipe,
    BibentryEditorComponent,
    BibliographyComponent,
    AuthorListComponent,
    AuthorComponent,
    IsbnDirective
  ],
  imports: [
    BrowserModule, FormsModule, AppRoutingModule, HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [RepositoryService, AuthorServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
